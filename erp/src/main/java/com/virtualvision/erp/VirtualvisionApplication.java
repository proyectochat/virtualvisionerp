package com.virtualvision.erp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VirtualvisionApplication {

	public static void main(String[] args) {
		SpringApplication.run(VirtualvisionApplication.class, args);
	}

}
